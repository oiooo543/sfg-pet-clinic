package com.bowen.sfgpetclinic.services;

import com.bowen.sfgpetclinic.model.Pet;


public interface PetService extends CrudService<Pet, Long> {


}
