package com.bowen.sfgpetclinic.services;

import com.bowen.sfgpetclinic.model.Owner;


public interface OwnerService extends  CrudService<Owner, Long>{

    Owner findByLastName(String lastName);


}
