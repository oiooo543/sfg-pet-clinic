package com.bowen.sfgpetclinic.services.map;

import com.bowen.sfgpetclinic.model.Owner;
import com.bowen.sfgpetclinic.model.Pet;
import com.bowen.sfgpetclinic.services.OwnerService;
import com.bowen.sfgpetclinic.services.PetService;
import com.bowen.sfgpetclinic.services.PetTypeService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OwnerServiceMap extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetTypeService petTypeService;
    private final PetService petService;


    public OwnerServiceMap(PetService petService, PetTypeService petTypeService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner object) {
        super.delete(object);
    }

    @Override
    public Owner save(Owner object) {
        Owner savedOwner = null;
        if (object != null) {
            if(object.getPets() != null) {
                object.getPets().forEach(pet -> {
                    if (pet.getPetType() != null){
                        if (pet.getPetType().getId() == null ){
                            pet.setPetType(petTypeService.save(pet.getPetType()));
                        }

                    } else {
                        throw new RuntimeException("Pet Type is required");
                    }

                    if (pet.getId() == null){
                        Pet savedpet = petService.save(pet);
                        pet.setId(savedpet.getId());
                    }
                });
            }
            return super.save(object);
        } else {
            return null;
        }
       // return super.save( object);
    }

    @Override
    public Owner findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Owner findByLastName(String lastName) {
        return null;
    }
}
