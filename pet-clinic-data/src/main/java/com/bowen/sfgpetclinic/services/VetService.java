package com.bowen.sfgpetclinic.services;

import com.bowen.sfgpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {


}
