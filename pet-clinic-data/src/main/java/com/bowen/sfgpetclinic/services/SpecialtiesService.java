package com.bowen.sfgpetclinic.services;

import com.bowen.sfgpetclinic.model.Specialty;

public interface SpecialtiesService extends CrudService<Specialty, Long> {
}
