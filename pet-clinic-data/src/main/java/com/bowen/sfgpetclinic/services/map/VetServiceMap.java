package com.bowen.sfgpetclinic.services.map;

import com.bowen.sfgpetclinic.model.Specialty;
import com.bowen.sfgpetclinic.model.Vet;
import com.bowen.sfgpetclinic.services.SpecialtiesService;
import com.bowen.sfgpetclinic.services.VetService;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class VetServiceMap extends  AbstractMapService<Vet, Long> implements VetService {

    private final SpecialtiesService specialtiesService;

    public VetServiceMap(SpecialtiesService specialtiesService) {
        this.specialtiesService = specialtiesService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);
    }

    @Override
    public Vet save(Vet object) {
        if (object.getSpecialty().size() > 0){
            object.getSpecialty().forEach(specialty -> {
                if(specialty.getId() == null ){
                    Specialty saveSpecialty = specialtiesService.save(specialty);
                    specialty.setId(saveSpecialty.getId());
                }
            });
        }
        return super.save(object);
    }

    @Override
    public Vet findById(Long id) {
        return super.findById(id);
    }
}
